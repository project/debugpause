<?php

namespace Drupal\debugpause\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Settings Form for the Debug Pause Module.
 */
class DebugpauseSettingsForm extends ConfigFormBase {

  /**
   * Private Route builder attribute.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  private $routeBuilder;

  /**
   * Injects routeBuilder as a dependency for rebuilding the routes.
   *
   * @param \Drupal\Core\Routing\RouteBuilderInterface $routeBuilder
   *   Route builder's interface for DI.
   */
  public function __construct(RouteBuilderInterface $routeBuilder) {
    $this->routeBuilder = $routeBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('router.builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'debugpause_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config('debugpause.settings');

    $form['pausein'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Amount of time before it pauses'),
      '#default_value' => $config->get('pausein'),
      '#description' => $this->t('Amount of time in Miliseconds before it pauses.'),
    ];

    $form['displaytitle'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display "Debug Pause" Title in the button.'),
      '#default_value' => $config->get('displaytitle'),
      '#description' => $this->t('Whether or not Title should be display in the button.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('debugpause.settings');
    $config->set('pausein', $form_state->getValue('pausein'));
    $config->set('displaytitle', $form_state->getValue('displaytitle'));
    $config->save();
    $this->routeBuilder->rebuild();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'debugpause.settings',
    ];
  }

}
