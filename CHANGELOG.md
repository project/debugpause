# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Release-Candidate]

## [1.0.0-rc1] - 2022-08-18
### Changed
- First release candidate of Debug Pause.

## [Pre-release]

## [1.0.0beta4] - 2022-08-08
### Added
- Added Text to README.txt.
- Added the option to enable or disable the text inside the button.

### Fixed
- Typo in the validateForm method.

## [1.0.0beta3] - 2022-08-05
### Added
- Added CHANGELOG.md to Repo.
- Rebuilds router on submitForm.

### Fixed
- Bug where the pausein value would not change because of caching.
