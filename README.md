# Debug Pause

The Debug Pause module displays an extra button in your admin toolbar
to pause the execution of javascript on your page,
providing developers an easy way to inspect elements
that disappear based on events.

 * For a full description of the module, visit the project page:
   [project page](https://www.drupal.org/project/debugpause)

 * To submit bug reports and feature suggestions, or track changes:
   [issue queue](https://www.drupal.org/project/issues/debugpause)


## Requirements

This module requires the following modules:

 * [Admin Toolbar](https://www.drupal.org/project/admin_toolbar)


## Installation

 * Install as you would normally install a contributed Drupal module. Visit 
   [steps of module installation](https://www.drupal.org/node/1897420) for further information.


## Configuration

 * Configure the user permissions in Administration » People » Permissions:

   - See and use the Debug Pause button (System module)

     The Debug button will need this permission so that it
     can be viewed and used. The Debug Pause button will not
     show up unless this permission is granted.

 * Customize the settings for the time before pausing in Administration »
   Configuration » Development » Debug Pause Settings menu.

 * To give other users the ability to use the button menu programmatically,
   you may assign them the "use debug pause" permission.


## Maintainers

Current maintainers:
 * Danton Mariano - [DantonMariano](https://www.drupal.org/u/dantonmariano)
