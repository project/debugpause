/**
 * @file
 * Provides Event listeners to button and Declares Debugger.
 */

(function($, Drupal) {
  Drupal.behaviors.debugpause = {
    attach: context => {
      once("debugpause", "html", context).forEach(function() {
        const $toolbarItem = $(
          ".toolbar-icon.toolbar-icon-debugpause-menu-link"
        );
        const $debugButton = $("#debugpausebutton");
        const seconds = $debugButton.attr("pausein");
        const isHidden = $debugButton.attr("ishidden");
        let isPausing = false;
        let pausing;
        let counting;

        $debugButton.click(() => {
          if (!isPausing) {
            isPausing = true;
            if (isHidden) {
              $debugButton.removeClass("admin-toolbar__menu__hidden");
            }
            let secondsInt = parseInt(seconds / 1000, 10);
            $toolbarItem.text(secondsInt);
            $toolbarItem.prop("title", "Cancels Pause");
            pausing = setTimeout(() => {
              $toolbarItem.text("Paused");
              debugger;
              clearInterval(counting);
              $toolbarItem.text("Debug Pause");
              $toolbarItem.prop("title", "Pauses Javascript");
              isPausing = false;
              if (isHidden) {
                $debugButton.addClass("admin-toolbar__menu__hidden");
                $toolbarItem.text("");
              }
            }, seconds);
            counting = setInterval(() => {
              secondsInt -= 1;
              $toolbarItem.text(secondsInt);
              if (secondsInt < 1) {
                $toolbarItem.text("Pausing...");
                clearInterval(counting);
              }
            }, 1000);
          } else {
            clearTimeout(pausing);
            clearInterval(counting);
            $toolbarItem.text("Cancelled");
            $toolbarItem.prop("title", "Pauses Javascript");
            setTimeout(() => {
              $toolbarItem.text("Debug Pause");
              isPausing = false;
              if (isHidden) {
                $debugButton.addClass("admin-toolbar__menu__hidden");
                $toolbarItem.text("");
              }
            }, 2000);
          }
        });
      });
    }
  };
})(jQuery, Drupal);
